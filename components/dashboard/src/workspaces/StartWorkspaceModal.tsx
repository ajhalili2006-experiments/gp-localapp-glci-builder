/**
 * Copyright (c) 2021 Gitpod GmbH. All rights reserved.
 * Licensed under the GNU Affero General Public License (AGPL).
 * See License-AGPL.txt in the project root for license information.
 */

import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import TabMenuItem from "../components/TabMenuItem";

export interface WsStartEntry {
    title: string
    description: string
    startUrl: string
}

interface StartWorkspaceModalProps {
    visible: boolean;
    recent: WsStartEntry[];
    examples: WsStartEntry[];
    selected?: Mode;
    onClose: () => void;
}

type Mode = 'Recent' | 'Examples';

    return (
        <Modal
            onClose={() => setIsStartWorkspaceModalVisible(false)}
            onEnter={() => false}
            visible={!!isStartWorkspaceModalVisible}
        >
            <h3 className="pb-2">Open in Gitpod</h3>
            <div className="border-t border-gray-200 dark:border-gray-800 mt-2 -mx-6 px-6 pt-4">
                <RepositoryFinder />
            </div>
        </Modal>
    );
}
