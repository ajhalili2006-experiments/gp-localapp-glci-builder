// Copyright (c) 2020 Gitpod GmbH. All rights reserved.
// Licensed under the GNU Affero General Public License (AGPL).
// See License-AGPL.txt in the project root for license information.

package cmd

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"

	supervisor "github.com/gitpod-io/gitpod/supervisor/api"
)

var credentialHelper = &cobra.Command{
	Use:    "credential-helper get",
	Short:  "Gitpod Credential Helper for Git",
	Long:   "Supports reading of credentials per host.",
	Args:   cobra.MinimumNArgs(1),
	Hidden: true,
	Run: func(cmd *cobra.Command, args []string) {
		action := args[0]
		log.SetOutput(io.Discard)
		f, err := os.OpenFile(os.TempDir()+"/gitpod-git-credential-helper.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if err == nil {
			defer f.Close()
			log.SetOutput(f)
		}
		if action != "get" {
			return
		}

		result, err := parseFromStdin()
		host := result["host"]
		if err != nil || host == "" {
			log.WithError(err).Print("error parsing 'host' from stdin")
			return
		}

		var user, token string
		defer func() {
			// Server could return only the token and not the username, so we fallback to hardcoded `oauth2` username.
			// See https://github.com/gitpod-io/gitpod/pull/7889#discussion_r801670957
			if token != "" && user == "" {
				user = "oauth2"
			}
			if token != "" {
				result["username"] = user
				result["password"] = token
			}
			for k, v := range result {
				fmt.Printf("%s=%s\n", k, v)
			}
		}()

		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
		defer cancel()
		supervisorAddr := os.Getenv("SUPERVISOR_ADDR")
		if supervisorAddr == "" {
			supervisorAddr = "localhost:22999"
		}
		supervisorConn, err := grpc.Dial(supervisorAddr, grpc.WithInsecure())
		if err != nil {
			log.WithError(err).Print("error connecting to supervisor")
			return
		}
		resp, err := supervisor.NewTokenServiceClient(supervisorConn).GetToken(ctx, &supervisor.GetTokenRequest{
			Host: host,
			Kind: "git",
		})
		if err != nil {
			log.WithError(err).Print("error getting token from supervisior")
			return
		}

		validator := exec.Command("/proc/self/exe", "git-token-validator",
			"--user", resp.User, "--token", resp.Token, "--scopes", strings.Join(resp.Scope, ","),
			"--host", host, "--repoURL", repoURL, "--gitCommand", gitCommand)
		err = validator.Start()
		if err != nil {
			log.WithError(err).Print("error spawning validator")
			return
		}
		err = validator.Process.Release()
		if err != nil {
			log.WithError(err).Print("error releasing validator")
			return
		}
		user = resp.User
		token = resp.Token
	},
}

func parseFromStdin() (map[string]string, error) {
	result := make(map[string]string)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if len(line) > 0 {
			tuple := strings.Split(line, "=")
			if len(tuple) == 2 {
				result[tuple[0]] = strings.TrimSpace(tuple[1])
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return result, nil
}

type gitCommandInfo struct {
	RepoUrl    string
	GitCommand string
}

func (g *gitCommandInfo) Ok() bool {
	return g.RepoUrl != "" && g.GitCommand != ""
}

var gitCommandRegExp = regexp.MustCompile(`git(?:\s+(?:\S+\s+)*)(push|clone|fetch|pull|diff|ls-remote)(?:\s+(?:\S+\s+)*)?`)
var repoUrlRegExp = regexp.MustCompile(`remote-https?\s([^\s]+)\s+(https?:[^\s]+)`)

// This method needs to be called multiple times to fill all the required info
// from different git commands
// For example from first command below the `RepoUrl` will be parsed and from
// the second command the `GitCommand` will be parsed
// `/usr/lib/git-core/git-remote-https origin https://github.com/jeanp413/test-gp-bug.git`
// `/usr/lib/git-core/git push`
func (g *gitCommandInfo) parseGitCommandAndRemote(cmdLineString string) {
	matchCommand := gitCommandRegExp.FindStringSubmatch(cmdLineString)
	if len(matchCommand) == 2 {
		g.GitCommand = matchCommand[1]
	}

	matchRepo := repoUrlRegExp.FindStringSubmatch(cmdLineString)
	if len(matchRepo) == 3 {
		g.RepoUrl = matchRepo[2]
		if !strings.HasSuffix(g.RepoUrl, ".git") {
			g.RepoUrl = g.RepoUrl + ".git"
		}
	}
	return host
}

func parseProcessTree() (repoUrl string, gitCommand string) {
	gitCommandRegExp := regexp.MustCompile(`git(,\d+\s+|\s+)(push|clone|fetch|pull|diff)`)
	repoUrlRegExp := regexp.MustCompile(`\sorigin\s*(https:[^\s]*)\s`)
	pid := os.Getpid()
	for {
		cmdLine := readProc(pid, "cmdline")
		if cmdLine == "" {
			return
		}
		cmdLineString := strings.ReplaceAll(cmdLine, string(byte(0)), " ")
		if gitCommand == "" {
			match := gitCommandRegExp.FindStringSubmatch(cmdLineString)
			if len(match) == 3 {
				gitCommand = match[2]
			}
		}
		if repoUrl == "" {
			match := repoUrlRegExp.FindStringSubmatch(cmdLineString)
			if len(match) == 2 {
				repoUrl = match[1]
				if !strings.HasSuffix(repoUrl, ".git") {
					repoUrl = repoUrl + ".git"
				}
			}
		}
		if repoUrl != "" && gitCommand != "" {
			return
		}
		statsString := readProc(pid, "stat")
		if statsString == "" {
			return
		}
		stats := strings.Fields(statsString)
		if len(stats) < 3 {
			log.Printf("Couldn't parse 3rd element from stats: '%s'", statsString)
			return
		}
		ppid, err := strconv.Atoi(stats[3])
		if err != nil {
			log.Printf("ppid '%s' is not a number", stats[3])
			return
		}
		if ppid == pid {
			return
		}
		pid = ppid
	}
}

func readProc(pid int, file string) string {
	procFile := fmt.Sprintf("/proc/%d/%s", pid, file)
	// read file not using os.Stat
	// see https://github.com/prometheus/procfs/blob/5162bec877a860b5ff140b5d13db31ebb0643dd3/internal/util/readfile.go#L27
	const maxBufferSize = 1024 * 512
	f, err := os.Open(procFile)
	if err != nil {
		log.WithError(err).Printf("Error opening %s", procFile)
		return ""
	}
	defer f.Close()
	reader := io.LimitReader(f, maxBufferSize)
	buffer, err := ioutil.ReadAll(reader)
	if err != nil {
		log.WithError(err).Printf("Error reading %s", procFile)
		return ""
	}
	return string(buffer)
}

// How to smoke test:
// - Open a public git repository and try pushing some commit with and without permissions in the dashboard, if no permissions a popup should appear in vscode
// - Open a private git repository and try pushing some commit with and without permissions in the dashboard, if no permissions a popup should appear in vscode
// - Private npm package
//   - Create a private git repository for an npm package e.g https://github.com/jeanp413/test-private-package
//   - Start a workspace, then run `npm install github:jeanp413/test-private-package` with and without permissions in the dashboard
// - Private npm package no access
//   - Open this workspace https://github.com/jeanp413/test-gp-bug and run `npm install`
//   - Observe NO notification with this message appears `Unknown repository '' Please grant the necessary permissions.`
// - Clone private repo without permission
//   - Start a workspace, then run `git clone 'https://gitlab.ebizmarts.com/ebizmarts/magento2-pos-api-request.git`, you should see a prompt ask your username and password, instead of `'gp credential-helper' told us to quit`
func init() {
	rootCmd.AddCommand(credentialHelper)
}
