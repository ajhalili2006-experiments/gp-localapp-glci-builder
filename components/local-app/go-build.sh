#!/usr/bin/env bash
set -ex

mkdir -p out || true

version="build-$(git rev-parse --short origin/ajhalili2006/develop).upstream-$(git rev-parse --short upstream/main)"
echo $version > ./version.txt

if [[ "$1" != "" ]] ; then
  TARGET_PLATFORM=$1
  TARGET_ARCH=$2
  GOOS=$TARGET_PLATFORM GOARCH=$TARGET_ARCH go build -x -v -o "./out/gp-localapp-$TARGET_PLATFORM-$TARGET_ARCH" ./main.go
else
  go build -o "./out/gp-localapp" -v -x ./main.go
fi

rm ./version.txt
