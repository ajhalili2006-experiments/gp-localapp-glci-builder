# Gitpod Local Companion CI Builder

Andrei Jiroh maintains an fork of Gitpod in GitLab SaaS with tweaks to Gitpod workspace configuration and even uses GitLab CI behind the scenes. This fork
is designed for building the Local Companion CLI for Linux users of both 32-bit and 64-bit Arm and x86-based CPUs.

## Meta

* Status: Experimental, not part of either Gitpodify project or Recap Time Squad OSS projects
* Canonical repo: <https://gitlab.com/ajhalili2006-experiments/gp-localapp-glci-builder>, with read-only mirrors at <https://github.com/ajhalili2006/gp-localapp-glci-builder>.
* License: Mixed, since it's forked from <https://github.com/gitpod-io/gitpod>, usually either MIT or AGPL. Small portions of the code contains Gitpod GmbH's custom license for EE-only use.

## Features

```bash
# Mostly supported by our CI builds, based on components/local-app/BUILD.yaml
linux/386
linux/amd64
linux/arm
linux/arm64
# Technically supported by the golang compilier, though we don't have builds for these arches,
#but we're open for these, especially if  requested by your distribution's package maintainer.
linux/mips
linux/mips64
linux/mips64le
linux/mipsle
linux/ppc64
linux/ppc64le
linux/riscv64
linux/s390x
```

## Building from source

Check the BUILD.md file for the instructions on how to build from source, including instructions on setting the version string before the build process.
